import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

import { Component, EventEmitter, Output } from '@angular/core';

import { RepoService } from '../repo/repo.service';


@Component({
  selector: 'gh-app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
  host: {
    'class':'search'
  }
})
export class SearchComponent {
  searchValue: string;

  @Output() private onSearch: EventEmitter<string> = new EventEmitter<string>();

  constructor(public _repoService: RepoService) {}

  clickHandler() {

    this.onSearch.emit(this.searchValue);

  }
}
