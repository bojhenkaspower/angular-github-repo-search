import 'rxjs/add/operator/map';

import { Inject, Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { API_URL } from '../shared/';

@Injectable()
export class RepoService {
  constructor(
    private _http: Http,
    @Inject(API_URL) private API_URL: string ) {}

  search(terms): Observable<any> {
    
    const searchURL = `${this.API_URL}/repositories?q=${terms}&sort=`;

    return this._http
      .get(searchURL)
      .map((response: Response) => response.json().items);
  }
}
