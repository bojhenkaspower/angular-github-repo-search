import { Component, Input } from '@angular/core';

import { Repository } from './repo';
import { GhAppComponent } from '../gh-app.component';

@Component({
  selector: 'gh-app-repository',
  templateUrl: './repo.component.html',
  styleUrls: ['./repo.component.scss'],
  host: {
    'class':'repository__layout'
  }
})
export class RepoComponent {
  public repositories: Repository[];
  constructor(private _ghAppComponent: GhAppComponent){

  }
  @Input() data: Repository;


}
