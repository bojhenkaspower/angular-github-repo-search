import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgxPaginationModule } from 'ngx-pagination';
import { TimeAgoPipe } from "./repo/time-ago-pipe.module";

import { GhAppComponent } from './gh-app.component';
import { RepoComponent } from './repo/repo.component';
import { RepoService } from './repo/repo.service';
import { SearchComponent } from './search/search.component';
import { API_URL } from './shared/';


@NgModule({
  declarations: [
    GhAppComponent,
    SearchComponent,
    RepoComponent,
    TimeAgoPipe
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    HttpModule,
    NgxPaginationModule
  ],
  providers: [
    { provide: API_URL, useValue: 'https://api.github.com/search' },
    RepoService
  ],
  bootstrap: [GhAppComponent]
})
export class GhAppModule { }
