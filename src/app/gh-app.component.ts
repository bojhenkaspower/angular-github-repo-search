import { Component } from '@angular/core';

import { Repository } from './repo/repo';
import { RepoService } from './repo/repo.service';



@Component({
  selector: 'gh-app',
  templateUrl: './gh-app.component.html',
  host: {
    'class': 'app-layout'
  }
})
export class GhAppComponent {
  public repositories: Repository[];
  p: number = 1;

  constructor(private _repoService: RepoService) {}

  trackReposBy(index: number, repository: Repository): number {
    return repository.id;
  }


  search(terms: string): void {

    this._repoService.search(terms)
      .subscribe((items: Repository[]) => this.repositories = items);
  }

  // isListAvailable() {
  //   function isArray(obj: any):boolean {
  //     return Array.isArray(obj);
  //   }
  //   return isArray(this.repositories) && this.repositories.length > 0;
  // }

  deleteItem(repo) {
    let index: number = this.repositories.indexOf(repo);
    if (index > -1) {
      this.repositories.splice(index, 1);
    }
  }
}
